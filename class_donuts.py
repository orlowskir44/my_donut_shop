from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField
from wtforms.validators import DataRequired, Email

TYPE = ['Basic','Berlin doughnuts','Lokma' , 'Sufganjot', 'Oliebollen' , 'Zeppole' , 'Bomboloni' , 'Healthy']

class Login(FlaskForm):
    email = StringField('Email',validators=[Email(),DataRequired()])
    passwd = StringField('Password',validators=[DataRequired()])

    submit = SubmitField('Submit')



class Create_donut(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    type = SelectField('Type', choices= TYPE,validators=[DataRequired()])
    ingredients = StringField('Ingredients', validators=[DataRequired()])
    preparation = StringField('Preparation', validators=[DataRequired()])
    price = StringField('Price', validators=[DataRequired()])

    submit = SubmitField('Create! ✅')