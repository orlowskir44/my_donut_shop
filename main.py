from flask import Flask, render_template, redirect,url_for
from flask_bootstrap import Bootstrap5
import csv
import class_donuts as cd
# pip install email_validator

app = Flask(__name__)
app.config['SECRET_KEY'] = '8BYkEfBA6O6donzWlSihBXox7C0sKR6b'
Bootstrap5(app)



@app.route('/')
def home():
    return render_template('index.html')



@app.route('/create', methods=['GET','POST'])
def create():
    create = cd.Create_donut()
    if create.validate_on_submit():
        with open('create.csv', 'a' , encoding='utf-8') as data:
            data.write(f'\n{create.name.data},'
                       f'{create.type.data},'
                       f'{create.ingredients.data},'
                       f'{create.preparation.data},'
                       f'{create.price.data}')
        return redirect(url_for('ideas'))
    return render_template('create_donut.html', donuts = create)


@app.route('/ideas')
def ideas():

    with open('create.csv', newline='', encoding='utf-8') as data:
        file = csv.reader(data)
        ideas = []
        for row in file:
           ideas.append(row)
    return render_template('ideas.html', ideas=ideas)



@app.route('/recipes')
def recipes():
    with open('create_donuts_pl.csv', newline='', encoding='utf-8') as data:
        file = csv.reader(data)
        donuts = []
        for row in file:
            donuts.append(row)
    return render_template('recipes.html',donuts = donuts)


@app.route('/shop')
def shop():
    return render_template('shop.html')


@app.route('/login',methods=['GET','POST'])
def login():
    login = cd.Login()
    text = ''
    if login.validate_on_submit():
        if login.email.data == 'admin@admin.pl' and login.passwd.data == '1234':
            text = 'Correct! work in progress...'
    return render_template('/login.html', login = login, message = text)


if __name__ == '__main__':
    app.run(debug=True,port=8000)